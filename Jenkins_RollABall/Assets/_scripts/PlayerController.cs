﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;

    Rigidbody rb;

    public int count;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

        //adding speed up method
        if (Input.GetKeyDown(KeyCode.Space)) //if spaced bar is pressed
            speed = speed * 2f; //speed doubled
        if (Input.GetKeyUp(KeyCode.Space)) //if space bar is released
            speed = speed / 2f; //speed returned to normal
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))

            //allows items to be collected and adds them to CountText
            other.gameObject.SetActive(false);
            count = count + 1;
        SetCountText();
    }
    void SetCountText()
    {
        //determines displayed CountText
        countText.text = "Count: " + count.ToString();
        if (count >= 8)
            //determines displayed WinText
            winText.text = "Winner!";


    }
}
