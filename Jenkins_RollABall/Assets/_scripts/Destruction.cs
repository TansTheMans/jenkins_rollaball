﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruction : MonoBehaviour
{
    PlayerController pc; //references the PlayerController script
     public int countRequirement = 8; //sets collectible count before floor disappears
    
    
    
    // Start is called before the first frame update
    void Start()
    {
       GameObject player = GameObject.FindGameObjectWithTag("Player"); //searches for the player object
       pc = player.GetComponent<PlayerController>(); //takes the player object script
    }

    // Update is called once per frame
    void Update()
    {
        if (pc.count >= countRequirement) //determines count required to destroy
        {
           Destroy(gameObject); //destroys floor
        }
            
    }
}
